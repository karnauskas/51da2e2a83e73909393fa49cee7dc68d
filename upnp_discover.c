#include<arpa/inet.h>
#include<linux/in.h>
#include<stdio.h>
#include<string.h>
#include<sys/socket.h>
#include<sys/types.h>

int main(int argc , char *argv[]) {
	int s;

	s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (s == -1) {
		printf("Could not create socket");
	}

	struct timeval timeout={2,0};
	setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(struct timeval));

	char *msg;
	msg = "M-SEARCH * HTTP/1.1\r\n"
	      "HOST:239.255.255.250:1900\r\n"
	      "ST:upnp:rootdevice\r\n"
	      "MX:2\r\n"
	      "MAN:\"ssdp:discover\"\r\n"
	      "\r\n";

	struct sockaddr_in to;
        memset(&to, 0, sizeof(to));
        to.sin_family = AF_INET;
	to.sin_addr.s_addr = inet_addr("239.255.255.250");
	to.sin_port = htons(1900);

	sendto(s, msg, strlen(msg), 0, (struct sockaddr*)&to, sizeof(to));


	char reply[10000];

	while(1) {
		int r = recv(s, reply, sizeof(reply), 0);
		if (r < 0) break;
		printf("%s\n---\n", reply);
	}

	return 0;
}